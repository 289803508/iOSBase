
import Foundation


class StringTools {
    
    //获取字符串首字母大写
    public func Metter(str:String) -> String {
        let strCap = str.capitalized
        let index = strCap.index(strCap.startIndex, offsetBy: 1)
        let prefix = strCap.substring(to: index)
        return prefix
    }
    
    
}
