#
# Be sure to run `pod lib lint iOSBase.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'iOSBase' #与github上创建的一致，代码块的名称
  s.version          = '0.1.0' #代码块的版本
  s.summary          = 'A short description of iOSBase.' #项目的摘要
  s.description      = <<-DESC
            #这里填写项目的描述，建议要写的比上面的摘要长，不然会有警告
            这是项目的基础库，功能主要包括网络请求，图片处理，工具类，所有项目都基于基础库构建开发
                       DESC

  s.homepage         = 'https://github.com/wangshujun123/iOSModleBase' #github仓库的下载地址
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' } #对应我们在创建仓库的时候选择的MIT License，授权许可文件
  s.author           = { '289803508@qq.com' => '289803508@qq.com' } #这里显示作者名称和邮箱，可以选择致谢一个名称，你自己随便写一个就可以
  #填写github仓库的下载地址，后面的照写就可以
  s.source           = { :git => 'https://github.com/wangshujun123/iOSModleBase.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '9.0'

  s.source_files = 'iOSBase/Classes/**/*'
  
  #如果是swift工程 记得往里面 增加 spec.swift_versions='3.0' 或者spec.swift_version = '3.0', '4.0'也可以spec.swift_versions = ['3.0']或者spec.swift_versions = ['3.0', '4.0', '5.0'] 否则会出现验证通不过报错。
  spec.swift_versions = ['3.0', '4.0', '5.0']
  
  # s.resource_bundles = {
  #   'iOSBase' => ['iOSBase/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
