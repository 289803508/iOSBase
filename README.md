# iOSBase

[![CI Status](https://img.shields.io/travis/289803508@qq.com/iOSBase.svg?style=flat)](https://travis-ci.org/289803508@qq.com/iOSBase)
[![Version](https://img.shields.io/cocoapods/v/iOSBase.svg?style=flat)](https://cocoapods.org/pods/iOSBase)
[![License](https://img.shields.io/cocoapods/l/iOSBase.svg?style=flat)](https://cocoapods.org/pods/iOSBase)
[![Platform](https://img.shields.io/cocoapods/p/iOSBase.svg?style=flat)](https://cocoapods.org/pods/iOSBase)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

iOSBase is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'iOSBase'
```

## Author

289803508@qq.com, 289803508@qq.com

## License

iOSBase is available under the MIT license. See the LICENSE file for more info.
